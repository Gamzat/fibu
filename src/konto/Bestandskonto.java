package konto;

public class Bestandskonto extends Konto {

    private double anfangsbestand;
    private boolean isPassive;

    /**
     * Erstellt ein Bestandskonto
     *
     * @param name
     * @param anfangsbestand
     * @param isPassive
     */
    public Bestandskonto(String name, double anfangsbestand, boolean isPassive) {
        super(name);
        this.anfangsbestand = anfangsbestand;
        this.isPassive     = isPassive;
    }


    /**
     * Gibt an, ob es ein Passivkonto ist
     *
     * @return boolean
     */
    public boolean isPassive() {
        return isPassive;
    }

    /**
     * Gibt den Anfangsbestand zur?ck
     *
     * @return double
     */
    public double getAnfansbestand() {
        return anfangsbestand;
    }
}
