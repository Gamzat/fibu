package konto;

public class Erfolgskonto extends Konto {

    private boolean isAufwand;

    /**
     * Erstellt ein Erfolgskonto
     *
     * @param name
     * @param isAufwand
     */
    public Erfolgskonto(String name, boolean isAufwand) {
        super(name);
        this.isAufwand = isAufwand;
    }

    /**
     * Gibt an ob es ein Aufwandskonto ist
     *
     * @return boolean
     */
    public boolean isAufwand() {
        return isAufwand;
    }
}
