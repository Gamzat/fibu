package konto;

import java.io.*;
import java.util.ArrayList;

public class Konten implements Serializable {

    private ArrayList<Konto> konten;

    /**
     * Konstruktor um eine leere Liste zu erzeugen
     */
    public Konten() {

        konten = new ArrayList<>();
    }

    /**
     * fügt ein Konto der Liste hinzu
     *
     * @param konto
     */
    public void addKonto(Konto konto) {

        if(getKonto(konto.getName()) == null) {

            konten.add(konto);
        }
    }

    /**
     * Gibt ein Konto zurück, wenn es exestiert
     * Exestiert das Konto nicht wird null zurückgegeben
     *
     * @param name
     * @return Konto
     */
    public Konto getKonto(String name) {
        for (Konto konto : konten) {
            if (konto.getName().equals(name)) return konto;
        }

        return null;
    }

    /**
     * Speichert alle Konten
     *
     * @param path
     */
    public void saveKonten(String path) {

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File(path)));
            out.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Ladet alle Konten
     *
     * @param path
     * @return
     * @throws Exception
     */
    public static Konten loadKonten (String path) throws Exception {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(path)));
        return (Konten) in.readObject();

    }

    /**
     * Gibt eine Liste vom Typ String mit den Kontennamen zurück
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getKontenAsList() {

        ArrayList<String> konten = new ArrayList<>();

        for (Konto konto : this.konten) {

            konten.add(konto.getName());

        }

        return  konten;

    }

    /**
     *
     * Gibt eine Liste welche die Konten enthält zurück
     *
     * @return ArrayList<Konto>
     */
    public ArrayList<Konto> getKonten() {
        return konten;
    }
}
