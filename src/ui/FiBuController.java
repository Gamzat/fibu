package ui;

import buchung.Buchung;
import buchung.Buchungen;
import buchung.Datum;
import gruppe.Gruppe;
import gruppe.Gruppen;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import konto.Bestandskonto;
import konto.Erfolgskonto;
import konto.Konten;
import konto.Konto;
import user.User;
import user.Users;

import java.io.Serializable;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class FiBuController implements Initializable {

    private static Konten konten;
    private static Users users;
    private static Gruppen gruppen;
    private static Buchungen buchungen;

    @FXML private TableView<TableViewController> historyTable;
    @FXML private TableColumn<TableViewController, String> historyTableDate;
    @FXML private TableColumn<TableViewController, String> historyTableNr;
    @FXML private TableColumn<TableViewController, String> historyTableSoll;
    @FXML private TableColumn<TableViewController, String> historyTableHaben;
    @FXML private TableColumn<TableViewController, String> historyTableRefKonto;
    @FXML private ComboBox<String> cbKonten;

    @FXML private Menu administration;
    @FXML private MenuItem abschl;

    /**
     * Füllt die Combobox
     */
    private void fillCBKonten() {

        ObservableList<String> items = FXCollections.observableArrayList(
                this.konten.getKontenAsList());

        this.cbKonten.setItems(items);
    }

    /**
     * Öffnet das Buchungsfenster
     */
    public  void runBuchen() {
        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("buchen/Buchen.fxml"));
            primaryStage.setTitle("Buchen");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Initialisiert Daten
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            buchungen = Buchungen.loadBuchungen("buchungen");
        } catch (Exception e) {
            buchungen = new Buchungen();
        }
        try {

            konten = Konten.loadKonten("konten");
        } catch (Exception e) {
            generateKonten();
        }

        try {
            users = Users.loadUsers("nutzer");
        } catch (Exception e) {
            users = new Users();
        }

        try {
            gruppen = Gruppen.loadGroups("gruppen");
        } catch (Exception e) {
            gruppen = new Gruppen();
        }


        historyTableDate.setCellValueFactory(new PropertyValueFactory<TableViewController, String>("datum"));
        historyTableNr.setCellValueFactory(new PropertyValueFactory<TableViewController, String>("bNr"));
        historyTableSoll.setCellValueFactory(new PropertyValueFactory<TableViewController, String>("sollWert"));
        historyTableHaben.setCellValueFactory(new PropertyValueFactory<TableViewController, String>("habenWert"));
        historyTableRefKonto.setCellValueFactory(new PropertyValueFactory<TableViewController, String>("refKonten"));


        fillCBKonten();
        cbKonten.setValue(cbKonten.getItems().get(0));
        refreshTableHistroy(getHostory(cbKonten.getValue()));

        cbKonten.getSelectionModel().selectedItemProperty().addListener( new ChangeListener<String>()  {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                refreshTableHistroy(getHostory(newValue));
            }
        });


        User user = users.getUser(FiBu.LOGGED_USERNAME);
        user.getGruppen().forEach(groupID -> {
            Gruppe group = gruppen.getGruppe(groupID);
            administration.setDisable(!group.getGruppenrechte().isSiehtUserManagement());
            abschl.setDisable(!group.getGruppenrechte().isKannAbschl());
        });
    }

    /**
     * Öffnet das Nutzerfenster
     *
     */
    public void runUser() {

        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("user/User.fxml"));
            primaryStage.setTitle("Nutzermanagement");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Erstellung des Jahresabschlusses bestehend aus SBK und GuV
     */
    public void abschluss() {
        Datum d = new Datum();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+2:00"));
        String zeit = formatter.format(new Date());
        String guvKontoName = "*Gewinn und Verlustrechnung " + Integer.toString(d.getJahr()) + "-" + Integer.toString(d.getMonat()) + "-" + Integer.toString(d.getTag()) + ": " + zeit;
        String sbkKontoName = "*Schlussbilanzkonto " + Integer.toString(d.getJahr()) + "-" + Integer.toString(d.getMonat()) + "-" + Integer.toString(d.getTag()) + ": " + zeit;
        String ebkKontoName = "*Anfangsbestand";
        FiBuController.getKonten().addKonto(new Bestandskonto(sbkKontoName, 0, false));
        FiBuController.getKonten().addKonto(new Bestandskonto(guvKontoName, 0, false));
        FiBuController.getKonten().addKonto(new Bestandskonto(ebkKontoName, 0, false));

        konten.getKonten().forEach(konto -> {
            if(istNichtTeilVomJahresabschluss(konto.getName())) {
                double saldo = konto.abschließen();
                if(saldo != 0) {
                    if (konto instanceof Erfolgskonto) {
                        if (((Erfolgskonto) konto).isAufwand()) {
                            // wenn Aufwandskonto:
                            abschlussBuchung(guvKontoName, konto.getName(), saldo);
                            konto.clearWerte();
                        } else {
                            // wenn Ertragskonto:
                            abschlussBuchung(guvKontoName, konto.getName(), saldo);
                            konto.clearWerte();
                        }
                    } else {
                        if (((Bestandskonto) konto).isPassive()) {
                            // wenn Passivkonto:
                            abschlussBuchung(sbkKontoName, konto.getName(), saldo);
                            konto.clearWerte();
                            abschlussBuchung(konto.getName(), ebkKontoName, saldo);
                        } else {
                            // wenn Aktivkonto:
                            abschlussBuchung(sbkKontoName, konto.getName(), saldo);
                            konto.clearWerte();
                            abschlussBuchung(konto.getName(), ebkKontoName, saldo);
                        }
                    }
                }
            }
        });

        FiBuController.getKonten().getKonten().remove(FiBuController.getKonten().getKonto(ebkKontoName));

        FiBuController.getKonten().saveKonten("konten");
        FiBuController.getBuchungen().saveBuchungen("buchungen");
        fillCBKonten();

    }

    /**
     * überPrüfung ob das Konto ein Teil eines Belibigen vorher gezogenem Jahresabschlusses entspricht
     * @param nameDesKontos
     * @return
     */
    private boolean istNichtTeilVomJahresabschluss(String nameDesKontos) {
        try {
            if (nameDesKontos.charAt(0) != '*') {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    /**
     * führt die Abschlussbuchung aus
     *
     * @param sollName
     * @param habenName
     * @param saldo
     */
    private void abschlussBuchung(String sollName, String habenName,double saldo) {
        Map<String, Double> habenKonto = new HashMap<>();
        Map<String, Double> sollKonto = new HashMap<>();
        if(saldo < 0) {
            String tmpKontoName = sollName;
            sollName = habenName;
            habenName = tmpKontoName;
            saldo = (saldo * -1);
        }
        sollKonto.put(sollName, saldo);
        habenKonto.put(habenName, (saldo * -1));
        FiBuController.getBuchungen().addBuchung(sollKonto, habenKonto);
    }


    /**
     * Öffnet das Gruppenfenster
     *
     */
    public void runGroup() {

        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("gruppe/Gruppe.fxml"));
            primaryStage.setTitle("Gruppenmanagement");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * Öffnet das Gruppenrechtefenster
     */
    public void runGruppenrechte() {

        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("gruppe/rechte/Gruppenrechte.fxml"));
            primaryStage.setTitle("Gruppenrechte");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Öffnet das Benutzergruppenfenster
     */
    public void runUserGroup() {

        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("user/Usergroups.fxml"));
            primaryStage.setTitle("Benutzergruppen");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Aktualisiert die History
     *
     * @param list
     */
    private void refreshTableHistroy(ArrayList<TableViewController> list) {

        ObservableList<TableViewController> items = FXCollections.observableArrayList(list);
        historyTable.setItems(items);

    }

    /**
     * Stellt die History zusammen und gibt sie zurück
     *
     * @param kontoname
     * @return ArrayList<TableViewController>
     */
    private ArrayList<TableViewController> getHostory(String kontoname) {
        ArrayList<TableViewController> history = new ArrayList<>();

        DecimalFormat df2 = new DecimalFormat("#0.00 €");


        for (Buchung buchung : buchungen.getBuchungen(kontoname)) {

            String sollWert  = "";
            String habenWert = "";
            String refKonten = "";

            for (Map.Entry<String, Double> entry : buchung.getSollKonten().entrySet()) {
                String konto = entry.getKey();
                double value = entry.getValue();

                if(konto.equals(kontoname)){
                    sollWert = df2.format(value);
                }
                else refKonten += konto + ", ";

            }

            for (Map.Entry<String, Double> entry : buchung.getHabenKonten().entrySet()) {
                String konto = entry.getKey();
                double value = entry.getValue();

                if(konto.equals(kontoname)){
                    habenWert = df2.format((value*-1));
                }
                else refKonten += konto + ", ";

            }

            if(refKonten.length() > 0) refKonten = refKonten.substring(0,refKonten.length()-2);

            history.add(new TableViewController(buchung.getDate().toString(), Integer.toString(buchung.getBuchungsNummer()), sollWert, habenWert, refKonten));
        }

        return history;
    }

    /**
     * Hier werden alle Konten erstellt
     *
     */
    private void generateKonten() {

        this.konten = new Konten();
        konten.addKonto(new Bestandskonto("(2) A I 1: Selbst geschaffene Imm. Vermögensgegenst.", 0, false));
        konten.addKonto(new Bestandskonto("(2) A I 2: entgeltl. erworbene Imm. Vermögensgegenst.", 0, false));
        konten.addKonto(new Bestandskonto("(2) A I 3: Geschäfts- oder Firmenwert", 0, false));
        konten.addKonto(new Bestandskonto("(2) A I 4: geleistete Anzahlungen", 0, false));
        konten.addKonto(new Bestandskonto("(2) A II 1: Grundstücke", 0, false));
        konten.addKonto(new Bestandskonto("(2) A II 2: techn. Anlagen und Maschienen", 0, false));
        konten.addKonto(new Bestandskonto("(2) A II 3: andere Anlagen und BGA", 0, false));
        konten.addKonto(new Bestandskonto("(2) A II 4: geleistete Anzahlungen und Anlagen im Bau", 0, false));
        konten.addKonto(new Bestandskonto("(2) A III 1: Anteile an verb. Unternehmen", 0, false));
        konten.addKonto(new Bestandskonto("(2) A III 2: Ausl. an verb. Unternehmen", 0, false));
        konten.addKonto(new Bestandskonto("(2) A III 3: Beteiligungen", 0, false));
        konten.addKonto(new Bestandskonto("(2) A III 4: Ausl. an Unternhemen in einem Beteiligungsverh.", 0, false));
        konten.addKonto(new Bestandskonto("(2) A III 5: Wertpapiere des Anlagevermögens", 0, false));
        konten.addKonto(new Bestandskonto("(2) A III 6: Sonstige Ausleihungen", 0, false));
        konten.addKonto(new Bestandskonto("(2) B I 1: Roh-, Hilfs- und Betriebsstoffe", 0, false));
        konten.addKonto(new Bestandskonto("(2) B I 2: unfertige Erzeugnisse, unfertige Leistungen", 0, false));
        konten.addKonto(new Bestandskonto("(2) B I 3: fertige Erzeugnisse und Waren", 0, false));
        konten.addKonto(new Bestandskonto("(2) B I 4: geleistete Anzahlungen", 0, false));
        konten.addKonto(new Bestandskonto("(2) B II 1: Forderungen aus L. u. L.", 0, false));
        konten.addKonto(new Bestandskonto("(2) B II 2: Forderungen gegen verb. Unternehmen", 0, false));
        konten.addKonto(new Bestandskonto("(2) B II 3: Forderungen gegeb Unternhemen in einem Beteiligungsverh.", 0, false));
        konten.addKonto(new Bestandskonto("(2) B II 4: sonstige Vermögensgegenstände", 0, false));
        konten.addKonto(new Bestandskonto("(2) B III 1: Anteile an verb. Unternehmen", 0, false));
        konten.addKonto(new Bestandskonto("(2) B III 2: sonstige Wertpapiere", 0, false));
        konten.addKonto(new Bestandskonto("(2) B IV: Kassenbestand, Bankguthaben, Guthaben bei Kred.", 0, false));
        konten.addKonto(new Bestandskonto("(2) C: Aktiver Rechnungsabgrenzungsposten", 0, false));
        konten.addKonto(new Bestandskonto("(2) D: Aktive latente Steuern", 0, false));
        konten.addKonto(new Bestandskonto("(2) E: Aktiver Unterschiedsbetrag aus der Vermögensverrechnung", 0, false));
        konten.addKonto(new Bestandskonto("(3) A I: Gezeichnetes Kapital", 0, true));
        konten.addKonto(new Bestandskonto("(3) A II: Kapitalrücklage", 0, true));
        konten.addKonto(new Bestandskonto("(3) A III 1: gesetziche Rücklage", 0, true));
        konten.addKonto(new Bestandskonto("(3) A III 2: Rücklage für Anteile an einem herrschendem Unternehmen", 0, true));
        konten.addKonto(new Bestandskonto("(3) A III 3: satzungsmäßige Rücklagen", 0, true));
        konten.addKonto(new Bestandskonto("(3) A III 4: andere Gewinnrücklagen", 0, true));
        konten.addKonto(new Bestandskonto("(3) A IV: Gewinnvortrag/Verlustvortrag", 0, true));
        konten.addKonto(new Bestandskonto("(3) A V: Jahresüberschuß/Jahresfehlbetrag", 0, true));
        konten.addKonto(new Bestandskonto("(3) B 1: Rückstellungen für Pensionen und ähnliche Verpflichtungen", 0, true));
        konten.addKonto(new Bestandskonto("(3) B 2: Steuerrückstellungen", 0, true));
        konten.addKonto(new Bestandskonto("(3) B 3: sonstige Rückstellungen", 0, true));
        konten.addKonto(new Bestandskonto("(3) C 1: Anleihen", 0, true));
        konten.addKonto(new Bestandskonto("(3) C 2: Verb. gegenüber Kreditinstituten", 0, true));
        konten.addKonto(new Bestandskonto("(3) C 3: erhaltene Anzahlungen auf Bestellungen", 0, true));
        konten.addKonto(new Bestandskonto("(3) C 4: Verb. aus L. u. L.", 0, true));
        konten.addKonto(new Bestandskonto("(3) C 5: Verb. aus angenommenen/ausgestellten Wechseln", 0, true));
        konten.addKonto(new Bestandskonto("(3) C 6: Verb. gegenüber verbundenen Unternehmen", 0, true));
        konten.addKonto(new Bestandskonto("(3) C 7: Verb. gegenüber Unternehmen in einem Beteiligungsverh.", 0, true));
        konten.addKonto(new Bestandskonto("(3) C 8: sonstige Verbindlichkeiten", 0, true));
        konten.addKonto(new Bestandskonto("(3) D: Passive Rechnungsabgrenzungsposten", 0, true));
        konten.addKonto(new Bestandskonto("(3) E: Passive latente Steuern", 0, true));
        konten.addKonto(new Erfolgskonto("(GUV) 1: Umsatzerlöse", false));
        konten.addKonto(new Erfolgskonto("(GUV) 2 A: Erhöhung des Bestandes an fertigen und unfertigen Erzeugnissen", false));
        konten.addKonto(new Erfolgskonto("(GUV) 2 B: Verminderungen des Bestandes an fertigen und unfertigen Erzeugnissen", true));
        konten.addKonto(new Erfolgskonto("(GUV) 3: andere aktivierte Eigenleistungen", false));
        konten.addKonto(new Erfolgskonto("(GUV) 4: sonstige betriebliche Erträge", false));
        konten.addKonto(new Erfolgskonto("(GUV) 5 A: Aufw. für Roh-, Hilfs- und Betriebsstoffe u. bez. Waren", true));
        konten.addKonto(new Erfolgskonto("(GUV) 5 B: Aufwendungen für bezogene Leistungen", true));
        konten.addKonto(new Erfolgskonto("(GUV) 6 A: Löhne und Gehälter", true));
        konten.addKonto(new Erfolgskonto("(GUV) 6 B: soz. Abg. u. Aufw. für Altersvers. u. für Unterstützung", true));
        konten.addKonto(new Erfolgskonto("(GUV) 7 A: Abschr. auf imm. Vermögensgegenst. des Anlagevermögens u. Sachanlagen", true));
        konten.addKonto(new Erfolgskonto("(GUV) 7 B: Abschr. auf Vermögensgegenstände des Umlaufvermögens", true));
        konten.addKonto(new Erfolgskonto("(GUV) 8: sonstige betriebliche Aufwendungen", true));
        konten.addKonto(new Erfolgskonto("(GUV) 9: Erträge aus Beteiligungen", false));
        konten.addKonto(new Erfolgskonto("(GUV) 10: Erträge aus anderen Wertpapieren und Ausl. des Finanzanlagevermögens", false));
        konten.addKonto(new Erfolgskonto("(GUV) 11: sonstige Zinsen und ähnliche Erträge", false));
        konten.addKonto(new Erfolgskonto("(GUV) 12: Abschr. auf Finanzanlagen und auf Wertpapiere des Umlaufvermögens", true));
        konten.addKonto(new Erfolgskonto("(GUV) 13: Zinsen und ähnliche Aufwendungen", true));
        konten.addKonto(new Erfolgskonto("(GUV) 14: Steuern vom Einkommen und vom Ertrag", true));
        konten.addKonto(new Erfolgskonto("(GUV) 16: sonstige Steuern", true));
        konten.saveKonten("konten");


    }


    /**
     * Gibt den Container für Konten zurück
     *
     * @return Konten
     */
    public static Konten getKonten() {

        return konten;

    }

    /**
     * Gibt den Container für Buchungen zurück
     *
     * @return Buchungen
     */
    public static Buchungen getBuchungen() {
        return buchungen;
    }

    /**
     * Gibt den Container für Nutzer zurück
     *
     * @return Users
     */
    public static Users getUsers() {return users;}

    /**
     * Gibt den Container für Gruppen zurück
     *
     * @return Gruppen
     */
    public static Gruppen getGroups() {return gruppen;}

    /**
     * Beendet das Programm
     */
    public void close() {

        ((Stage) historyTable.getScene().getWindow()).close();

    }


}
