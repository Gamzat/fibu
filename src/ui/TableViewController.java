package ui;

import buchung.Datum;

public class TableViewController {

    private String datum;
    private String bNr;
    private String sollWert;
    private String habenWert;
    private String refKonten;

    public TableViewController(String datum, String bNr, String sollWert, String habenWert, String refKonten) {
        this.datum = datum;
        this.bNr = bNr;
        this.sollWert = sollWert;
        this.habenWert = habenWert;
        this.refKonten = refKonten;
    }

    public String getDatum() {
        return datum;
    }

    public String getBNr() {
        return bNr;
    }

    public String getSollWert() {
        return sollWert;
    }

    public String getHabenWert() {
        return habenWert;
    }

    public String getRefKonten() {
        return refKonten;
    }
}
