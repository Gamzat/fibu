package ui.gruppe;

import gruppe.Gruppe;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ui.FiBu;
import ui.FiBuController;
import user.User;

import java.net.URL;
import java.util.ResourceBundle;

public class GruppeController implements Initializable {

    private String currentGroupname;

    @FXML private TextField groupID;
    @FXML private TextField groupname;
    @FXML private ListView<String> gruppenListe;

    /**
     * Initialisiert Daten
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fillList();
        gruppenListe.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {

                    fillGroupData(newValue);
                    currentGroupname = newValue;
                }
            }
        });

    }

    /**
     * Füllt die Gruppenliste
     */
    private void fillList() {
        ObservableList<String> items = FXCollections.observableArrayList(
                FiBuController.getGroups().getGroupsAsList());

        this.gruppenListe.setItems(items);
    }

    /**
     * Überprüft ob die ID schon vergeben ist
     *
     * @return boolean
     */
    private boolean checkGroupID() {

        for (Gruppe gruppe : FiBuController.getGroups().getGruppen()) {
            if (gruppe.getId() == Integer.parseInt(groupID.getText())) return false;
        }

        return true;
    }

    /**
     * Legt eine neue Gruppe an
     */
    public void addGroup() {

        Alert alert = new Alert(Alert.AlertType.ERROR);

        if (FiBuController.getGroups().getGruppe(groupname.getText()) == null && checkGroupID()) {
            if (!groupID.getText().equals("")
                    && !groupname.getText().equals("")) {

                FiBuController.getGroups().addGruppe(new Gruppe(Integer.parseInt(groupID.getText()), groupname.getText()));
                fillList();
                currentGroupname = groupname.getText();
            } else {
                alert.setTitle("Pflichtfelder");
                alert.setHeaderText("Bitte alle Felder ausfüllen");
                alert.showAndWait();
            }
        } else {
            alert.setAlertType(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Gruppe oder Gruppen-ID exestiert bereits!");
            alert.showAndWait();
        }
    }

    /**
     * Löscht die aktuell ausgewählte Gruppe
     */
    public void deleteGroup() {

        FiBuController.getGroups().deleteGruppe(currentGroupname);
        fillList();

    }


    /**
     * Speichert die aktuellen Nutzerwerte
     */
    public void save() {

        FiBuController.getGroups().saveGroups("gruppen");
        fillList();

    }


    /**
     * Aktuallisiert die Textfelder
     *
     * @param groupname
     */
    private void fillGroupData(String groupname) {
        Gruppe gruppe = FiBuController.getGroups().getGruppe(groupname);
        groupID.setText(Integer.toString(gruppe.getId()));
        this.groupname.setText(gruppe.getName());

    }


    /**
     * Schließt das Gruppenfenster
     */
    public void close() {

        ((Stage) gruppenListe.getScene().getWindow()).close();

    }


}
