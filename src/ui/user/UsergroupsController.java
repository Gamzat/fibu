package ui.user;

import gruppe.Gruppe;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import ui.FiBuController;
import user.User;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UsergroupsController implements Initializable {

    private String currentItemGroup;
    private String currentItemUserGroup;

    @FXML private ComboBox<String> user;
    @FXML private ListView<String> groups;
    @FXML private ListView<String> userGroups;



    /**
     * Initialisiert Daten
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fillUser();
        if (!user.getItems().isEmpty()) {
            user.setValue(user.getItems().get(0));
            fillLists();
        }


        groups.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                currentItemGroup = newValue;
            }
        });

        userGroups.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                currentItemUserGroup = newValue;
            }
        });


    }

    /**
     * Füllt die User-ComboBox
     */
    private void fillUser() {

        ObservableList<String> items = FXCollections.observableArrayList(
                FiBuController.getUsers().getUsersAsList());

        user.setItems(items);

    }

    /**
     * Füllt die Listen
     */
    public void fillLists() {
        User user = FiBuController.getUsers().getUser(this.user.getValue());
        ArrayList<String> groups = FiBuController.getGroups().getGroupsAsList();
        ArrayList<String> userGroups = user.getGroupsAsList();



        userGroups.forEach(userGroup -> {
            groups.remove(userGroup);
        });

        ObservableList<String> itemsUser = FXCollections.observableArrayList(
                userGroups);

        ObservableList<String> itemsGroup = FXCollections.observableArrayList(
                groups);


        this.userGroups.setItems(itemsUser);
        this.groups.setItems(itemsGroup);



    }


    /**
     * Bei einem Doppelklick Wird die Gruppe der anderen Liste hinzugefügt
     *
     * @param mouseEvent
     */
    public void moveKonto(MouseEvent mouseEvent) {

        if (mouseEvent.getClickCount() == 2) {
            switch (((ListView) mouseEvent.getSource()).getId()) {
                case "groups":
                    if (!currentItemGroup.equals("")) {
                        userGroups.getItems().add(currentItemGroup);
                        groups.getItems().remove(currentItemGroup);
                        currentItemGroup = groups.getSelectionModel().getSelectedItem();
                    }

                    break;
                case "userGroups":
                    if (!currentItemUserGroup.equals("")) {
                        groups.getItems().add(currentItemUserGroup);
                        userGroups.getItems().remove(currentItemUserGroup);
                        currentItemUserGroup = userGroups.getSelectionModel().getSelectedItem();
                    }
                    break;
            }

        }
    }

    /**
     * Speichert die Usergruppen
     */
    public void save() {


        if (!userGroups.getItems().isEmpty()) {

            User user;
            int groupID;
            for (Object userGroup : userGroups.getItems()) {

                user = FiBuController.getUsers().getUser(this.user.getValue());
                groupID = FiBuController.getGroups().getGruppe((String)userGroup).getId();
                user.addGruppe(groupID);
            }
        } else {
            FiBuController.getUsers().getUser(user.getValue()).getGruppen().clear();
        }


        FiBuController.getUsers().saveUsers("nutzer");

    }

    /**
     * Schließt das Fenster
     */
    public void close() {

        ((Stage) user.getScene().getWindow()).close();

    }
}
