package user;

import ui.FiBuController;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {

    private String loginname;
    private String password;
    private String name;
    private ArrayList<Integer> gruppen;

    /**
     * Konstruktor um einen Nutzer zu erstellen
     *
     * @param loginname
     * @param name
     * @param password
     */
    public User(String loginname, String name, String password) {
        this.loginname = loginname;
        this.password  = password;
        this.name      = name;
        gruppen        = new ArrayList<>();
    }

    /**
     * Gibt den Loginnamen zurück
     *
     * @return String
     */
    public String getLoginname() {
        return loginname;
    }

    /**
     * Ändert den Loginnamen
     *
     * @param loginname
     */
    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    /**
     * Gibt den Benutzernamen zurück
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Ändert den Benutzernamen
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Fügt den Nuter einer Gruppe hinzu
     * @param id
     */
    public void addGruppe(int id) {
        if (!gruppen.contains(id)) gruppen.add(id);
        gruppen.forEach(gruppe -> {
            System.out.println(gruppe);
        });
    }

    /**
     * Gibt die Gruppen als Liste zurück.
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getGroupsAsList() {

        ArrayList <String> groups = new ArrayList<>();

        for (int id : gruppen) {
            groups.add(FiBuController.getGroups().getGruppe(id).getName());
        }

        return groups;

    }

    /**
     *
     * Gibt die Gruppenids als ArrayList zurück
     *
     * @return ArrayList<Integer>
     */
    public ArrayList<Integer> getGruppen() {
        return gruppen;
    }

    public String getPassword() {
        return password;
    }

}
