package validation;

import user.User;
import user.Users;

public class Login {



    public static boolean validateLogin(String loginname, Users users){

        for (User user : users.getUsers()){

            if (user.getLoginname().trim().equals(loginname.trim())) return true;
        }

        return false;
    }
}
